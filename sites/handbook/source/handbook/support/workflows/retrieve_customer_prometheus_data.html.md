---
layout: markdown_page
title: Retrieve and load customer Prometheus data
category: Self-managed
description: How to retrieve and load customer prometheus data
---

## Retrieving the data from the customer

These steps are to be sent to the customer.  

1. Edit /opt/gitlab/sv/prometheus/run and add flag `--web.enable-admin-api` to the startup script to enable the admin API
1. Restart Prometheus with `gitlab-ctl restart prometheus` so that the flag takes effect
1. Take the snapshot with `curl -XPOST http://127.0.0.1:9090/api/v1/admin/tsdb/snapshot`
1. Find the latest snapshot folder with `ls -lrt /var/opt/gitlab/prometheus/data/snapshots`
1. Archive the snapshot with `tar czvf <snapshot-folder>.tar.gz /var/opt/gitlab/prometheus/data/snapshots/<snapshot-folder>`
1. Remove the `--web.enable-admin-api` flag from `/opt/gitlab/sv/prometheus/run` and restart Prometheus to disable the endpoint
1. Customer can now upload the snapshot to the Zendesk ticket.

# Loading the data into a local system
To load the snapshot data into Prometheus we can use a deployment of Omnibus-GitLab which bundles Prometheus

**On the Omnibus instance:**
1. Stop Prometheus with `gitlab-ctl stop prometheus`
1. Extract the contents of `<snapshot-folder>/*` into `/var/opt/gitlab/prometheus/data/`
1. Start Prometheus
1. Now the data is loaded you can query it using Grafana or a cli tool such as [promql-cli](https://github.com/nalbury/promql-cli).
