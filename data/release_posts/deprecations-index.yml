---
'17.0':
- Remove `previousStageJobsOrNeeds` from GraphQL
- GraphQL API access through unsupported methods
'16.9':
- Agent for Kubernetes option `ca-cert-file` renamed
- Secure analyzers major version update
- Rename the 'require_password_to_approve' field
- Upgrading the operating system version of GitLab SaaS runners on Linux
- SAST analyzer coverage changing in GitLab 17.0
- npm package uploads now occur asynchronously
- Scan execution policies enforcing scans with an `_EXCLUDED_ANALYZERS` variable will
  override project variables
- "'repository_download_operation' audit event type for public projects"
- Autogenerated Markdown anchor links with dash (`-`) characters
- Deprecating Windows Server 2019 in favor of 2022
- The `direction` GraphQL argument for `ciJobTokenScopeRemoveProject` is deprecated
- Deprecate license metadata format V1
- Deprecate Terraform CI/CD templates
- Deprecate `version` field in feature flag API
- Deprecate `fmt` job in Terraform Module CI/CD template
- Deprecate Python 3.9 in Dependency Scanning and License Scanning
- Support for self-hosted Sentry versions 21.4.1 and earlier
- Removal of tags from small SaaS runners on Linux
- "`omniauth-azure-oauth2` gem is deprecated"
- Maven versions below 3.8.8 support in Dependency Scanning and License Scanning
- Min concurrency and max concurrency in Sidekiq options
- Deprecate License Scanning CI/CD artifact report type
- Security policy field `match_on_inclusion` is deprecated
- Deprecate License Scanning CI templates
- Deprecate Grype scanner for Container Scanning
- "`dependency_files` is deprecated"
- Dependency Scanning incorrect SBOM metadata properties
- Deprecate custom role creation for group owners on self-managed
- Heroku image upgrade in Auto DevOps build
- Compliance framework in general settings
'16.8':
- GitLab Runner provenance metadata SLSA v0.2 statement
- "`after_script` keyword will run for cancelled jobs"
- License Scanning support for sbt 1.0.X
- Support for setting custom schema for backup is deprecated
- "`metric` filter and `value` field for DORA API"
- License List is deprecated
- Dependency Scanning support for sbt 1.0.X
'16.7':
- JWT `/-/jwks` instance endpoint is deprecated
- List repository directories Rake task
- 'Dependency Proxy: Access tokens to have additional scope checks'
'16.6':
- The GitHub importer Rake task
- 'GraphQL: deprecate support for `canDestroy` and `canDelete`'
- Proxy-based DAST deprecated
- File type variable expansion fixed in downstream pipelines
- Breaking change to the Maven repository group permissions
- Legacy Geo Prometheus metrics
- Container registry support for the Swift and OSS storage drivers
'16.5':
- Offset pagination for `/users` REST API endpoint is deprecated
- Security policy field `newly_detected` is deprecated
'16.4':
- "`postgres_exporter['per_table_stats']` configuration setting"
- Internal container registry API tag deletion endpoint
- 'Geo: Legacy replication details routes for designs and projects deprecated'
- Deprecate change vulnerability status from the Developer role
- The `ci_job_token_scope_enabled` projects API attribute is deprecated
'16.3':
- Twitter OmniAuth login option is removed from GitLab.com
- Deprecate field `hasSolutions` from GraphQL VulnerabilityType
- RSA key size limits
- 'Geo: Housekeeping Rake tasks'
- Twitter OmniAuth login option is deprecated from self-managed GitLab
- GraphQL field `totalWeight` is deprecated
- Job token allowlist covers public and internal projects
'16.2':
- GraphQL field `registrySizeEstimated` has been deprecated
- OmniAuth Facebook is deprecated
- Deprecated parameters related to custom text in the sign-in page
- Deprecate `CiRunner` GraphQL fields duplicated in `CiRunnerManager`
- The pull-based deployment features of the GitLab agent for Kubernetes is deprecated
'16.11':
- "`GITLAB_SHARED_RUNNERS_REGISTRATION_TOKEN` is deprecated"
'16.10':
- Behavior change for protected variables and multi-project pipelines
- "`omnibus_gitconfig` configuration item is deprecated"
- List container registry repository tags API endpoint pagination
- Duplicate storages in Gitaly configuration
'16.1':
- Deprecate Windows CMD in GitLab Runner
- GraphQL deprecation of `dependencyProxyTotalSizeInBytes` field
- Deprecate `message` field from Vulnerability Management features
- Unified approval rules are deprecated
- Running a single database is deprecated
'16.0':
- Changing MobSF-based SAST analyzer behavior in multi-module Android projects
- GitLab administrators must have permission to modify protected branches or tags
- GraphQL type, `RunnerMembershipFilter` renamed to `CiRunnerMembershipFilter`
- "`sidekiq` delivery method for `incoming_email` and `service_desk_email` is deprecated"
- PostgreSQL 13 no longer supported
- CiRunner.projects default sort is changing to `id_desc`
- Bundled Grafana deprecated and disabled
'15.9':
- Default CI/CD job token (`CI_JOB_TOKEN`) scope changed
- Queue selector for running Sidekiq is deprecated
- Legacy URLs replaced or removed
- Secure scanning CI/CD templates will use new job `rules`
- Trigger jobs can mirror downstream pipeline status exactly
- SAST analyzer coverage changing in GitLab 16.0
- Secure analyzers major version update
- Required Pipeline Configuration is deprecated
- Development dependencies reported for PHP and Python
- Support for Praefect custom metrics endpoint configuration
- Managed Licenses API
- "`omniauth-authentiq` gem no longer available"
- License-Check and the Policies tab on the License Compliance page
- License Compliance CI Template
- The GitLab legacy requirement IID is deprecated in favor of work item IID
- Enforced validation of CI/CD parameter character lengths
- Legacy Praefect configuration method
- The `Project.services` GraphQL field is deprecated
- CI/CD jobs will fail when no secret is returned from Hashicorp Vault
- Default CI/CD job token (`CI_JOB_TOKEN`) scope changed
- Embedding Grafana panels in Markdown is deprecated
- Option to delete projects immediately is deprecated from deletion protection settings
- GitLab Runner platforms and setup instructions in GraphQL API
- External field in Releases and Release Links APIs
- External field in GraphQL ReleaseAssetLink type
- Deprecation and planned removal for `CI_PRE_CLONE_SCRIPT` variable on GitLab SaaS
- Single database connection is deprecated
- HashiCorp Vault integration will no longer use CI_JOB_JWT by default
- Old versions of JSON web tokens are deprecated
- Slack notifications integration
'15.8':
- The Visual Reviews tool is deprecated
- Developer role providing the ability to import projects to a group
- The latest Terraform templates will overwrite current stable templates
- Use of third party container registries is deprecated
- The API no longer returns revoked tokens for the agent for Kubernetes
- Non-standard default Redis ports are deprecated
- Configuring Redis config file paths using environment variables is deprecated
- "`environment_tier` parameter for DORA API"
- Maintainer role providing the ability to change Package settings using GraphQL API
- Live Preview no longer available in the Web IDE
- Container registry pull-through cache
- Projects API field `operations_access_level` is deprecated
- Cookie authorization in the GitLab for Jira Cloud app
- GitLab Helm chart values `gitlab.kas.privateApi.tls.*` are deprecated
- Deployment API returns error when `updated_at` and `updated_at` are not used together
- Auto DevOps support for Herokuish is deprecated
- 'GraphQL: The `DISABLED_WITH_OVERRIDE` value of the `SharedRunnersSetting` enum
  is deprecated. Use `DISABLED_AND_OVERRIDABLE` instead'
- Limit personal access token and deploy token's access with external authorization
- Automatic backup upload using Openstack Swift and Rackspace APIs
- Dependency Scanning support for Java 13, 14, 15, and 16
- Auto DevOps no longer provisions a PostgreSQL database by default
- Conan project-level search endpoint returns project-specific results
- Azure Storage Driver defaults to the correct root prefix
'15.7':
- DAST report variables deprecation
- Shimo integration
- Support for periods (`.`) in Terraform state names might break existing states
- ZenTao integration
- "`POST ci/lint` API endpoint deprecated"
- The `gitlab-runner exec` command is deprecated
- KAS Metrics Port in GitLab Helm Chart
- DAST API scans using DAST template is deprecated
- DAST ZAP advanced configuration variables deprecation
- The Phabricator task importer is deprecated
- DAST API variables
- Support for REST API endpoints that reset runner registration tokens
'15.6':
- "`runnerRegistrationToken` parameter for GitLab Runner Helm Chart"
- Registration tokens and server-side runner arguments in `gitlab-runner register`
  command
- GitLab Runner registration token in Runner Operator
- Registration tokens and server-side runner arguments in `POST /api/v4/runners` endpoint
- Configuration fields in GitLab Runner Helm Chart
'15.5':
- vulnerabilityFindingDismiss GraphQL mutation
- File Type variable expansion in `.gitlab-ci.yml`
- GraphQL field `confidential` changed to `internal` on notes
'15.4':
- Container Scanning variables that reference Docker
- Toggle behavior of `/draft` quick action in merge requests
- Starboard directive in the configuration of the GitLab agent for Kubernetes
- Vulnerability confidence field
- Non-expiring access tokens
'15.3':
- Use of `id` field in vulnerabilityFindingDismiss mutation
- CAS OmniAuth provider
- Redis 5 deprecated
- Atlassian Crowd OmniAuth provider
- Security report schemas version 14.x.x
'15.2':
- Remove `job_age` parameter from `POST /jobs/request` Runner endpoint
'15.10':
- Environment search query requires at least three characters
- DingTalk OmniAuth provider
- Work items path with global ID at the end of the path is deprecated
- Major bundled Helm Chart updates for the GitLab Helm Chart
- Legacy Gitaly configuration method
- Deprecated Consul http metrics
- Bundled Grafana Helm Chart is deprecated
'15.1':
- PipelineSecurityReportFinding projectFingerprint GraphQL field
- PipelineSecurityReportFinding name GraphQL field
- Jira DVCS connector for Jira Cloud
'15.0':
- PostgreSQL 12 deprecated
'14.9':
- GitLab self-monitoring project
- Integrated error tracking disabled by default
- "`user_email_lookup_limit` API field"
- GraphQL permissions change for Package settings
- htpasswd Authentication for the container registry
- Permissions change for downloading Composer dependencies
- Background upload for object storage
'14.8':
- Querying Usage Trends via the `instanceStatisticsMeasurements` GraphQL node
- OAuth tokens without expiration
- Test coverage project CI/CD setting
- Dependency Scanning Python 3.9 and 3.6 image deprecation
- Secure and Protect analyzer major version update
- Out-of-the-box SAST support for Java 8
- Deprecate feature flag PUSH_RULES_SUPERSEDE_CODE_OWNERS
- SAST support for .NET 2.1
- Secure and Protect analyzer images published in new location
- Request profiling
- Vulnerability Check
- SAST analyzer consolidation and CI/CD template changes
- Container Network and Host Security
- Support for gRPC-aware proxy deployed between Gitaly and rest of GitLab
- "`started` iteration state"
- Runner `active` GraphQL fields replaced by `paused`
- GraphQL ID and GlobalID compatibility
- GraphQL networkPolicies resource deprecated
- Deprecate legacy Gitaly configuration methods
- Optional enforcement of SSH expiration
- "`CI_BUILD_*` predefined variables"
- Optional enforcement of PAT expiration
- Retire-JS Dependency Scanning tool
- "`projectFingerprint` in `PipelineSecurityReportFinding` GraphQL"
- External status check API breaking changes
- Elasticsearch 6.8
- Required pipeline configurations in Premium tier
'14.7':
- Monitor performance metrics through Prometheus
- Logging in GitLab
- Tracing in GitLab
- Sidekiq metrics and health checks configuration
- "`artifacts:reports:cobertura` keyword"
'14.6':
- apiFuzzingCiConfigurationCreate GraphQL mutation
- CI/CD job name length limit
- Legacy approval status names from License Compliance API
- "`type` and `types` keyword in CI/CD configuration"
- bundler-audit Dependency Scanning tool
'14.5':
- GraphQL API Runner status will not return `paused`
- "`pipelines` field from the `version` field"
- Package pipelines in API payload is paginated
- "`dependency_proxy_for_private_groups` feature flag"
- "`promote-to-primary-node` command from `gitlab-ctl`"
- Update to the container registry group-level API
- "`promote-db` command from `gitlab-ctl`"
- Known host required for GitLab Runner SSH executor
- "`Versions` on base `PackageType`"
- Support for SLES 12 SP2
- "`defaultMergeCommitMessageWithDescription` GraphQL API field"
- Changing an instance (shared) runner to a project (specific) runner
- SaaS certificate-based integration with Kubernetes
- Value Stream Analytics filtering calculation change
- Self-managed certificate-based integration with Kubernetes
'14.3':
- GitLab Serverless
- Audit events for repository push events
- OmniAuth Kerberos gem
- Legacy database configuration
'14.10':
- Dependency Scanning default Java version changed to 17
- Outdated indices of Advanced Search migrations
- Toggle notes confidentiality on APIs
'14.0':
- OAuth implicit grant
- Changing merge request approvals with the `/approvals` API endpoint
