---
features:
  primary:
  - name: "Understand your users better with Product Analytics"
    available_in: [ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/product_analytics/'
    video: 'https://www.youtube.com/embed/i8Mze9lRZiY'
    reporter: lfarina8
    stage: monitor # Prefix this file name with stage-informative-title.yml
    categories:
      - 'Product Analytics Visualization'
    epic_url: # Multiple links are supported. Avoid linking to confidential issues.
      - 'https://gitlab.com/groups/gitlab-org/-/epics/12716'
    description: |
      It is critical to understand how your users are engaging with your application in order to make data-driven decisions about future innovations and optimizations. Are you seeing an uptick in usage for your top business critical URLs, is there an unusual dip in monthly active users, are you seeing more customers engaging with a mobile Android device? By having the answers to questions like this and making them accessible to your engineering teams from the GitLab platform, your teams can stay in sync with how their development work is affecting user outcomes. 
      
      With GitLab's new Product Analytics feature, you can instrument your applications, collect key usage and adoption data about your users, and then display it inside GitLab. You can visualize data in dashboards, report on it, and filter it in a variety of different ways to find insights about your users. Your team can now quickly identify and respond to unexpected dips or spikes in customer usage that signify an issue, as well as celebrate the success of their recent releases.

      To use Product Analytics, you will need a Kubernetes cluster to install this [helm chart](https://gitlab.com/gitlab-org/analytics-section/product-analytics/helm-charts) and
      instrument your application to send traffic to it. GitLab will then connect to the cluster to retrieve the
      data for visualization.
