features:
  primary:
  - name: "Speed up your builds with the Maven dependency proxy"
    available_in: [premium, ultimate]
    documentation_link: 'https://docs.gitlab.com/ee/user/packages/package_registry/dependency_proxy/'
    video: 'https://www.youtube-nocookie.com/embed/9NPTXObsSrE?si=3LvALvBfc5YMNw5G'
    reporter: trizzi
    stage: package
    categories:
      - 'Dependency Proxy'
      - 'Package Registry'
    epic_url:
      - 'https://gitlab.com/groups/gitlab-org/-/epics/3610'
    description: |
      A typical software project relies on a variety of dependencies, which we call packages. Packages can be internally built and maintained, or sourced from a public repository. Based on our user research, we've learned that most projects use a 50/50 mix of public and private packages. Package installation order is very important, as using an incorrect package version can introduce breaking changes and security vulnerabilities into your pipelines.

      Now you can add one external Java repository to your GitLab project. After adding it, when you install a package using the dependency proxy, GitLab first checks for the package in the project. If it's not found, GitLab then attempts to pull the package from the external repository.

      When a package is pulled from the external repository, it's imported into the GitLab project. The next time that particular package is pulled, it's pulled from GitLab and not the external repository. Even if the external repository is having connectivity issues and the package is present in the dependency proxy, pulling the package still works, making your pipelines faster and more reliable.

      If the package changes in the external repository (for example, a user deletes a version and publishes a new one with different files) the dependency proxy detects it. It invalidates the package, so GitLab pulls the newer one. This ensures the correct packages are downloaded, and helps reduce security vulnerabilities.
