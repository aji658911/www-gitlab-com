---
features:
  secondary:
  - name: "Improved ability to keep the latest job artifacts"
    available_in: [core, premium, ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/ci/jobs/job_artifacts.html#keep-artifacts-from-most-recent-successful-jobs'
    reporter: jocelynjane
    stage: verify
    categories:
    - Build Artifacts
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/428408'
    description: |
      In GitLab 13.0 we introduced the ability to keep the job artifacts from the most recent successful pipeline. Unfortunately, the feature also marked all [failed](https://gitlab.com/gitlab-org/gitlab/-/issues/266958) and [blocked](https://gitlab.com/gitlab-org/gitlab/-/issues/387087) pipelines as the latest pipeline regardless of whether they were the most recent or not. This led to a buildup of artifacts in storage which had to be deleted manually.

      In GitLab 16.7 the bugs causing this unintended behavior are resolved. Job artifacts from failed and blocked pipelines are only kept if they are from the most recent pipeline, otherwise they will follow the `expire_in` configuration. Affected GitLab.com customers should see artifacts which were inadvertently kept now unlocked and removed after a new pipeline run.

      The **Keep artifacts from most recent successful jobs** setting overrides the job's `artifacts: expire_in` configuration and can result in a large number of artifacts stored without expiry. If your pipelines create many large artifacts, they can fill up your project storage quota quickly. We recommend disabling this setting if this feature is not required.
