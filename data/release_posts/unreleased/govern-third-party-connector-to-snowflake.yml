---
features:
  primary:
  - name: "GitLab connector application now available on the Snowflake Marketplace"
    available_in: [ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/integration/snowflake.html'
    image_url: '/images/unreleased/gitlab-snowflake-connector.png'
    reporter: khornergit
    stage: govern
    categories:
    - Audit Events
    - Compliance Management
    epic_url: 'https://gitlab.com/groups/gitlab-org/-/epics/13004'
    description: |
      Audit events are created and stored in GitLab. Before this release, audit events could only be accessed from in GitLab, with results reviewed using the GitLab UI.

      However, customers also wanted the ability to have audit events in third party destinations (such as SIEM solutions like Snowflake) to make it easier to:

      - See, combine, manipulate, and report on all of the audit event data from an organization's multiple systems, including GitLab.
      - Only look at specific audit events that they care about so that they can quickly answer the questions they are interested in.
      - Have a full picture of what goes on inside GitLab and be able to review it after the fact.

      To help customers with these tasks, we have created a GitLab connector application for the [Snowflake Marketplace](https://app.snowflake.com/marketplace), which uses the Audit events API.
      To make use of this functionality, customers must deploy and manage the application using the Snowflake Marketplace.
